#include "simpletools.h"
#include "servo.h"

void calibrate(); //This function causes the motor with the phototransistor to "scan" through angle 0 through 180 in increments of 10 and saves the angle at which the light is most intense
int getDecay();   //This function gets the decay time from the phototransistor. The more intense the light, a lower value with be returned
void compare();   //This compares the current decay time measurement to whichever was the previous best and sets the optimal angle (angleOpt) to the current angle if the light is more intense
void daytime();   //This serves as the daytime running process. The motor with the solar cell will move throughout the day. Once it reaches the limit, it will reset itself and wait until the following day

int angle, angleOpt, decayOpt, decay; //current angle, optimal angle, decay time at optimal angle, and decay time at current angle
int *cog;                             //This is the core that will run the daytime function

int main()  // main function
{
  calibrate();                  //calibrate the position of the solar cell
  cog = cog_run(daytime, 128);  //start the daytime function and have it run independently from the following while loop
  while(1) {
    if(input(7)) {                  //if the calibration button is pressed
      cog_end(cog);                 //halt the daytime function to prepare for recalibration
      calibrate();                  //recalibrate the position of the solar cell
      cog = cog_run(daytime, 128);  //continue the daytime function
    }
  }
}

void calibrate() {
  servo_angle(16, 0);       //initialize the motor to 0 degrees
  angleOpt = 0;             //set the optimal angle to the initial state
  decayOpt = getDecay();    //set to the decay time at the initial state 
  angle = 0;                //initialize the starting angle to 0 degrees
  pause(1000);              //allow time for the light sensor to reset back to 0 degrees
  while(angle < 1800) {     //repeat until the motor has turned 180 degrees
    servo_angle(16, angle); //move to next angle
    compare();              //call the compare function to compare the current results with the previous optimal state
    pause(50);              //wait 0.05 seconds as the compare
    angle += 100;           //set the next angle to the current angle + 10 degrees
  }
  servo_angle(16, angleOpt);  //after the sensor has "scanned" through the 180 degrees, move to the optimal angle                                
}

int getDecay() {
  high(5);              //set p5 high
  pause(1);             //wait for circuit to charge
  return rc_time(5, 1); //return the decay time (lower values indicate a more intense light)
}

void compare() {
  decay = getDecay();     //get the decay time at the current angle
  if(decay < decayOpt) {  //if the light at the current angle is more intense than at the previous optimal angle
    decayOpt = decay;     //set the current decay time to be the optimal decay time to be the current one
    angleOpt = angle;     //set the current angle to be the optimal angle
  }
}

void daytime() {
  servo_angle(14, angleOpt);  //set the solar cell position to the calibrated position
  angle = angleOpt;           //make sure that the angle variable starts at the calibrated position
  while(1) {
    while(angle < 1701) {     //170.1 degrees is chosen because we do not want the position to exceed 180 degrees
      angle += 100;           //set the next angle to the current angle + 10 degrees
      pause(5000);            //in a real world application this may be an hour, but for practical purposes 5 seconds will be used
      servo_angle(14, angle); //move to the next angle
    }
    angle = 0;              //set the angle to the reset position
    servo_angle(14, angle); //move the solar cell to the reset position
    pause(10000);           //in a real world application this may be overnight, but for practical purposes 10 seconds will be used       
  }
}