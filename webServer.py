from flask import Flask, redirect, url_for, render_template, request, flash
from werkzeug.utils import secure_filename
from flask import send_from_directory
import tablib
import os
import csv

UPLOAD_FOLDER = '/home/pi/FinalProject'
ALLOWED_EXTENSIONS = {'txt', 'csv', 'png', 'jpg', 'jpeg', 'gif', 'pdf'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
	return '.' in filename and \
            filename.split('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/uploads/<filename>')
def uploaded_file(filename):
	return send_from_directory(app.config['UPLOAD_FOLDER'], filename)

@app.route("/index.html", methods=['GET', 'POST'])
def upload_file():
	if request.method == 'POST':
		#check if the post request has the file part
		if 'file' not in request.files:
			flash('No file part')
			return redirect(request.url)
		file = request.files['file']
		# if user does not select file, browser also
		# submit an empty part without filename
		if file.filename == ' ':
			flash('No selected file')
			return redirect(request.url)
		if file and allowed_file(file.filename):
			filename = secure_filename(file.filename)
			file.save(os.path.join(app.config['UPLOAD_FOLDER'],filename))
		return redirect(url_for('uploaded_file',filename=filename))
	return render_template("index.html")

@app.route("/about.html")
def about():
    return render_template("about.html")

if __name__ == "__main__":
	app.run(debug=True)
